GlossaryCheck is a linguistic tool to help find terminology errors in large string based localization projects using spreadsheet files. Terminology translation errors can make texts more difficult to understand or can change the meaning. Without Computer-assisted translation software, it is difficult and time consuming to find terminology translation errors. Correct terminology is a feature of high quality translations, especially of more complex texts (e.g. philosophy or scientific texts, but also novels or texts in games). Imagine a special term in a philosophical text has several different translations...

GlossaryCheck works with bulk inputs of glossaries (up to 1048576 entries) and string files (up to 1048576 strings) via .xlsx spreadsheets. GlossaryCheck is independent of other Computer-assisted translation software and can be used as analyzing tool, analyzing the bulk output of several translators, collected in one single spreadsheet (e.g. selective output of large MySql databases). The output of GlossaryCheck is a .xlsx spreadsheet with detailed info about localization terminology errors (String ID, terminology term in source and localized language, original string, localized string).

GlossaryCheck lists strings, where the source string contains a terminology term and the translated string does not contain the corresponding term. In order to analyze the same text input from different perspectives, GlossaryCheck allows different sensitivity settings (case insensitive, but sensitive to word boundaries, case sensitive and sensitive to word boundaries, case sensitive, but insensitive to word boundaries...).

GlossaryCheck contains also tools to list all strings containing Terminology terms (GlossaryList), to check typos of numbers inside of strings and to create or extend terminology lists (GCCreator). 

GlossaryCheck is written by A.D.Klumpp using Python and the Python library openpyxl including jdcal and et_xmlfile 
(see license texts below or in the folders of the libraries). 
GlossaryCheck is released under the terms of the GNU General Public License (See http://www.gnu.org/licenses/) 
Copyright (C) 2015 A.D.Klumpp. 
GlossaryCheck is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY. 
The full copyright notices and the full license texts shall be included in all copies or substantial portions of the Software.
Alternative versions of GlossaryCheck are written by A.D. Klumpp using Python and the Python libraries openpyxl (including jdcal and et_xmlfile), XlsxWriter and kivy (thanks to python-for-android). 

Python is released under the Python Software Foundation License 
(see https://www.python.org/download/releases/2.7.6/license/). 
Openpyxl is released under MIT/Expat license 
(see https://openpyxl.readthedocs.org/en/latest/). 
Kivy is released under the MIT License 
(see https://github.com/kivy/kivy/blob/master/LICENSE). 
XlsxWriter is released under a BSD license (Copyright (c) 2013, John McNamara 
(http://xlsxwriter.readthedocs.org/license.html).
jdcal is released under BSD 
(see https://pypi.python.org/pypi/jdcal).
et_xmlfile is released under MIT 
(Home-page: https://bitbucket.org/openpyxl/et_xmlfile).

Please read the full license texts Online or in the LICENSES.txt document, which is inside the GlossaryCheck folder.

